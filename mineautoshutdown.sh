#!/bin/bash
#    Copyright (C) 2023 zyphlar
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

STATUS_URL="http://example.com/minecraft.php"
PASSWORD="your_password_here"
status=$(curl -s -X POST -F "action=status" "$STATUS_URL?password=$PASSWORD")

if [[ $status =~ "running" ]]; then

  if [[ $status =~ "0 players" ]]; then

    if [ ! -f "/tmp/minecount" ]; then
      touch /tmp/minecount
    fi

    count=$(cat /tmp/minecount)

    if [[ $count -gt 6 ]]; then
      echo "$status count $count, killing"
      curl -s -X POST -F "action=off" "$STATUS_URL?password=$PASSWORD"
      echo "0" > /tmp/minecount
    else
      echo $(($count+1)) > /tmp/minecount
      echo "$status count $count, waiting"
    fi
  else
    echo "$status, resetting"
    echo "0" > /tmp/minecount
  fi
else
  echo "not active: $status"
fi
