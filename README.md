# Minecraft server automatic startup and shutdown

A collection of scripts to start a Minecraft server on Amazon AWS on demand
and automatically shut it down when not in use.

![Screenshot of software showing a Turn On button and server status](screenshot.png)

## Installation and Usage

These packages require PHP, Python3, the PIP module mcstatus, the AWS cli, and a webserver.
Install this on a server different from your minecraft server, since you'll be shutting that down.

Run as root: `python3 -m pip install mcstatus`

Install the .php file on a webserver and configure the variables as desired.

Place the remaining files the same server in the `/var/minecraft/` folder
and use CRON to run the shell script every 5 minutes:

`*/5 *   * * *  root   /var/minecraft/mineautoshutdown.sh 2>&1 | logger`

Ensure the same password is present in the php script and the shutdown script.
Review all other files to make sure passwords/keys/hostnames/regions are correct.

Call the php script via HTTP using the correct password when players want to play:

`curl -X POST -F "action=on" http://your.webserver.here/minecraft.php?password=your_password_here`

Or just direct them to <http://your.webserver.here/minecraft.php?password=your_password_here>

If the CRON job runs properly and has all the right variables/permissions/dependencies,
it will shut down the DO droplet about 30-40 minutes after no players are detected in the game.

### AWS Setup

Install Minecraft Java Edition on an AWS instance. Give it an Elastic IP so it stays consistent.
Set Minecraft to start on boot and ensure it has the correct ports open in its Security Group.

Create an IAM policy with these permissions (edit the instance ID to be your minecraft instance)

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ec2:StartInstances",
                "ec2:StopInstances"
            ],
            "Resource": "arn:aws:ec2:*:*:instance/your_instance_id_here"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "ec2:DescribeInstanceStatus",
            "Resource": "*"
        }
    ]
}
```

Create an IAM user attached to that policy and create an access key for it.
On a the server where you've downloaded Minecraft-Minder, save the key
and id to `/var/www/.aws/credentials` (assuming you use Ubuntu/Apache) with:

```
[default]
aws_access_key_id = AKIA_your_key_id_here
aws_secret_access_key = your_secret_key_here
```

Finally `chown -r www-data:www-data /var/www/.aws/`

## License

    Copyright (C) 2023 zyphlar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.