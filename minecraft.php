<?php

$REGION="us-west-1"; // change as needed
$INSTANCE_ID="your_instance_id_here";
$PASSWORD="your_password_here";
if ($_POST['action'] == "on") {

  $out = `aws ec2 start-instances --region=$REGION --instance-id $INSTANCE_ID 2>&1`;

} elseif ($_POST['action'] == "off") {

  $out = `aws ec2 stop-instances --region=$REGION --instance-id $INSTANCE_ID`;

} elseif ($_POST['action'] == "status") {

  $status = `aws ec2 describe-instance-status --region=$REGION --include-all-instances --instance-id $INSTANCE_ID 2>&1`;
  $json = json_decode($status, true);

  $statusOut = $json['InstanceStatuses'][0]['InstanceState']['Name'];

  if ($json['InstanceStatuses'][0]['InstanceState']['Name'] == "running") {
    $players = `python3 /var/minecraft/minestatus.py 2>&1`;
    $json = json_decode($players, true);
    if ($json['players'] !== null) {
      $statusOut .= " with ".$json['players']." players";
    } else {
      $statusOut .= " but not reachable yet.";
    }
  }

  exit($statusOut);

}


if ($out) {
  $json = json_decode($out, true);

  if ($_POST['action'] == "on") {
    echo "<a href='javascript:history.back()'>Go back</a><br/><br/><b>Result:</b> ";
  }

  if ($json['StartingInstances']) {
    exit($json['StartingInstances'][0]['CurrentState']['Name']);
  } elseif ($json['StoppingInstances']) {
    exit($json['StoppingInstances'][0]['CurrentState']['Name']);
  } elseif ($json['InstanceStatuses']) {
    exit($json['InstanceStatuses'][0]['InstanceState']['Name']);
  }
}

?><!DOCTYPE html><html>
<!--
    Copyright (C) 2023 zyphlar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->
<head>
  <title>Minecraft Server Control</title>
  <meta name="generator">Minecraft-Minder</meta>
  <style type="text/css">
    @keyframes spinning {
      from { transform: rotate(360deg) }
      to { transform: rotate(0deg) }
    }
    .spin {
      animation-name: spinning;
      animation-duration: 3s;
      animation-iteration-count: infinite;
      /* linear | ease | ease-in | ease-out | ease-in-out */
      animation-timing-function: linear;
    }
  </style>
</head>
<body>
<p>
  Press this button to turn on the server. It'll take a couple minutes to fully turn on,
  and it'll turn off automatically after about 30 minutes of inactivity.
</p>
<form action="#" method="POST">
  <input type="hidden" name="action" value="on" />
  <input type="submit" value="Turn On" />
</form>
<br/>
<b>Current status:</b>
<i id="statusOut"></i><svg id="spinner" class="spin" style="display:none; height: 1em; margin-left: 1em;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve"><g><path d="M355.1,137.7C355.1,67.2,412.3,10,482.9,10c70.5,0,127.7,57.2,127.7,127.7c0,70.5-57.2,127.7-127.7,127.7C412.3,265.4,355.1,208.3,355.1,137.7z M761.2,520.9L761.2,520.9c0-57.9,46.9-104.8,104.8-104.8S970.8,463,970.8,520.9l0,0c0,57.9-46.9,104.8-104.8,104.8S761.2,578.8,761.2,520.9z M658.9,791.8c0-52.4,42.5-94.9,94.9-94.9c52.4,0,94.9,42.5,94.9,94.9c0,52.4-42.5,94.9-94.9,94.9C701.4,886.7,658.9,844.2,658.9,791.8z M396.9,904c0-47.5,38.5-86,86-86s86,38.5,86,86c0,47.5-38.5,86-86,86S396.9,951.5,396.9,904L396.9,904z M134.1,791.8c0-43,34.9-77.8,77.8-77.8c43,0,77.8,34.9,77.8,77.8c0,43-34.9,77.8-77.8,77.8C168.9,869.7,134.1,834.8,134.1,791.8z M29.2,520.9c0-38.9,31.6-70.5,70.5-70.5c38.9,0,70.5,31.6,70.5,70.5s-31.6,70.5-70.5,70.5C60.8,591.4,29.2,559.8,29.2,520.9z M148.1,250L148.1,250c0-35.3,28.6-63.9,63.9-63.9c35.3,0,63.9,28.6,63.9,63.9l0,0c0,35.3-28.6,63.9-63.9,63.9C176.7,313.8,148.1,285.2,148.1,250z M869.5,250c0,63.9-51.8,115.7-115.7,115.7S638.1,313.8,638.1,250s51.8-115.7,115.7-115.7C817.7,134.3,869.5,186.1,869.5,250z"/></g></svg>

<script>
  function loadStatus(){

    document.getElementById("spinner").style.display = "inline";

    let url = document.location;
    const formData = new FormData();
    formData.append('action', 'status');

    let options = {
        method: "POST",
        body: formData
    }

    fetch(url, options)
    .then(res => res.text())
    .then(body => {
      console.log('Checkout this resp: ', body);
      document.getElementById("statusOut").innerText = body;
      document.getElementById("spinner").style.display = "none";
    })
    .catch(err => { throw err });

  }

  setInterval(function(){ loadStatus(); }, 30000);
  loadStatus();

</script>
</body>
</html>
